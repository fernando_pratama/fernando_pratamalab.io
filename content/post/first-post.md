+++
author = "Fernando Pratama"
date = "2017-05-14T01:58:42+08:00"
tags = ["blog"]
title = "Starting a Personal Blog"
description = "Introduction"
draft = false

+++
I decided to create personal blog for documenting what I have learnt so far from online courses.
The content will be mostly about Data Science and Functional Programming.
I have interest on application of Deep Learning on NLP and Clojure programming language.
